/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//- Thermal and Mechanical solver selection:
thermalSolver           solidConduction;
mechanicsSolver         smallStrain;
neutronicsSolver        diffusion;
elementTransport        fromLatestTime;

//- Material and rhelogy treatment:
materialProperties      byZone;
rheology                byMaterial;

heatSource              timeDependentLhgr;
burnup                  Lassmann;
fastFlux                timeDependentAxialProfile;
corrosion               fromLatestTime;
gapGas                  FRAPCON;
fgr                     SCIANTIX;
sliceMapper             autoAxialSlices;
corrosion               fromLatestTime;

globalOptions
{
    pinDirection            (0 0 1);
    reactorType             "LWR";
    angularFraction         0.25;
}

thermalSolverOptions
{
    heatFluxSummary     off;
}

rheologyOptions
{
    thermalExpansion on;
    planeStress     on;
}

mechanicsSolverOptions
{
    forceSummary        off;  
    cylindricalStress   on;  

    multiMaterialCorrection
    {
        type                    uniform;
        defaultWeights          1;
    }
}

fgrOptions
{
    nFrequency  1;
    relax       1;
}

gapGasOptions
{
    gapPatches ( fuelOuter cladInner );
    holePatches ();
    topFuelPatches    ( top);
    bottomFuelPatches ( bottom);

    gapVolumeOffset 0.0;
    gasReserveVolume 0.0;
    gasReserveTemperature 290;
}

heatSourceOptions
{
    timePoints  ( 0  3600     1.26E+08 );
    lhgr        ( 0  200e2    100e2 );
    timeInterpolationMethod linear;

    axialProfile
    {
        type flat;
    }

    radialProfile
    {
        type    fromBurnup;
    }

    materials ( fuel );
}

fastFluxOptions
{
    timePoints  ( 0  3600     1.26E+08 );
    fastFlux    ( 0  2e13     1e13 );
    timeInterpolationMethod linear;

    axialProfile
    {
        type flat;
    }

    materials ( fuel cladding );
}


materials
{
    fuel
    {
        material                    UO2;
        Tref                        Tref [ 0 0 0 1 0 ] 293;

        densificationModel          UO2FRAPCON;
        swellingModel               UO2FRAPCON;
        relocationModel             UO2FRAPCON;

        enrichment                  0.045;
        rGrain                      2.8e-05;
        GdContent                   0.0;
        theoreticalDensity          10960;
        densityFraction             0.95;
        dishFraction                0.0;

        resinteringDensityChange    0.3;
        GapCold                     0.168e-3;
        DiamCold                    8.468e-3;
        recoveryFraction            0.5;
        outerPatch                  "fuelOuter";

        isotropicCracking           on;
        nCracksMax                  12;

        rheologyModel               elasticity;
    }

    cladding
    {
        material                zircaloy;
        Tref                    Tref [ 0 0 0 1 0 ] 293;

        PoissonRatioModel ZyConstant;

        rheologyModel   misesPlasticCreep;
        rheologyModelOptions
        {
            plasticStrainVsYieldStress table
            (
                (0    250e6)
            );

            creepModel LimbackCreepModel;
            relax 1.0;
        }
    }
}

// ************************************************************************* //
