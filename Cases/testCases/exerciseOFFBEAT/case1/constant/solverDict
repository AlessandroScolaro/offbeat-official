/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// Thermal and Mechanical solver selection:
thermalSolver           solidConduction;
mechanicsSolver         fromLatestTime;
neutronicsSolver        fromLatestTime;
elementTransport        fromLatestTime;

// Material and rhelogy treatment:
materialProperties      byZone;
rheology                byMaterial;

heatSource              timeDependentLhgr;
burnup                  none;
fastFlux                fromLatestTime;
corrosion               fromLatestTime;
gapGas                  none;
fgr                     none;
sliceMapper             autoAxialSlices;


globalOptions
{
    pinDirection            (0 0 1);
    reactorType             "LWR";
}


heatSourceOptions
{
    timePoints  ( );
    lhgr        ( );
    timeInterpolationMethod linear;

    axialProfile
    {
        type flat;
    }

    radialProfile
    {
        type    flat;
    }

    materials ( fuel );
}


materials
{
    fuel
    {
        material                    UO2;

        conductivityModel           constant;
        k                           ;

        Tref                        Tref [ 0 0 0 1 0 ] 293;

        enrichment                  0.045;
        rGrain                      2.8e-05;
        GdContent                   0.0;
        theoreticalDensity          10960;
        densityFraction             0.95;
        dishFraction                0.0;

        densificationModel          none;
        swellingModel               none;
        relocationModel             none;
        isotropicCracking           off;

        rheologyModel               elasticity;
    }

    cladding
    {
        material                    zircaloy;
        Tref                        Tref [ 0 0 0 1 0 ] 293;        

        conductivityModel           constant;
        k                           ;

        swellingModel               none;
        phaseTransitionModel        none;

        rheologyModel               elasticity;
    }
}

// ************************************************************************* //
