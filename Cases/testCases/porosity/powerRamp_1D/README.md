# 1D OFFBEAT Porosity Migration Solver

## Overview

This README provides a concise guide for the 1D OFFBEAT case, showcasing the capabilities of the Porosity Migration Solver. The case involves a UO2 solid cylindrical pellet subjected to a 7-hour irradiation. Power is increased at a rate of 10 kW/m/min for 7 minutes and then maintained at a constant level.

## Usage

To simulate the case, run the following command:

```bash
./Allrun
```

## Results

Upon completion, the radial distribution of porosity, conductivity, power density, and temperature will be plotted in the `results.pdf` file.

