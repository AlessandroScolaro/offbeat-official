/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//- Select the physics to solve for
thermalSolver           solidConduction;
mechanicsSolver         smallStrain;
neutronicsSolver        fromLatestTime;
elementTransport        fromLatestTime;

materialProperties      byZone;
rheology                byMaterial;

heatSource              fromLatestTime;
burnup                  fromLatestTime;
fastFlux                fromLatestTime;
sliceMapper             byMaterial;
gapGas                  FRAPCON;
fgr                     none;
corrosion               fromLatestTime;

globalOptions
{
    pinDirection            (0 0 1);
    reactorType             "LWR";
}

thermalSolverOptions
{
    heatFluxSummary     off;
}

rheologyOptions
{
    thermalExpansion     on;
    planeStress off;
    modifiedPlaneStrain on;    
    coolantPressureList 
    {
        values          
        2
        (
            (0 0)
            (1e10 0)
        );
    }
    springModulus   3500;
}

mechanicsSolverOptions
{
    forceSummary        off;  
    cylindricalStress   on;  
    RhieChowCorrection  false;
}

gapGasOptions
{
    gapPatches ( fuelOuter cladInner );
    holePatches ();
    topFuelPatches ( fuelTop );
    bottomFuelPatches ( fuelBottom );
    
    gapVolumeOffset 0.0;
    gasReserveVolume 0.0;
    gasReserveTemperature 290;

    includeCentralHole off;
    includeDishes off;
}

materials
{
    fuel
    {
        material                    constant;

        Tref                        Tref        [ 0 0 0 1 0 ]   293;
        rho                         rho         [1 -3 0 0 0]    10500;
        Cp                          Cp          [0 2 -2 -1 0]   296.3;
        k                           k           [1 1 3 -1 0]    3;
        emissivity                  emissivity  [0 0 0 0 0]     0.8707;
        E                           E           [1 -1 -2 0 0]   2e+11;
        nu                          nu          [0 0 0 0 0]     3.16e-01;
        alpha                       alpha       [0 0 0 0 0]     9.89e-6;

        isotropicCracking           off;
        nSlices                     10;
        rheologyModel               elasticity;
    }

    cladding
    {
        material                    constant;        

        rho                         rho         [1 -3 0 0 0]    6560;
        Cp                          Cp          [0 2 -2 -1 0]   285;
        k                           k           [1 1 3 -1 0]    21.5;
        emissivity                  emissivity  [0 0 0 0 0]     0.808642;
        E                           E           [1 -1 -2 0 0]   9.93e10;
        nu                          nu          [0 0 0 0 0]     0.37;
        alpha                       alpha       [0 0 0 0 0]     6e-6;
        Tref                        Tref        [0 0 0 1 0]     293;


        nSlices                 12;
        heightSlices (0.1 0.29 0.29 0.29 0.29 0.29 0.29 0.29 0.29 0.29 0.29 0.2);

        rheologyModel               elasticity;
    }
}

// ************************************************************************* //
