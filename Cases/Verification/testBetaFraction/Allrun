#! /bin/sh

# Source tutorial run functions
. "$WM_PROJECT_DIR/bin/tools/RunFunctions"

rm -f *.png

# Set simulation
runApplication -o foamDictionary 0/T -entry internalField -set "uniform 900" 
runApplication -a foamDictionary 0/T -entry boundaryField/cladOuter/temperatureSeries/values -set "((0 900) (50 1400) (100 900))" 
runApplication -a foamDictionary 0/T -entry boundaryField/cladOuter/value -set "uniform 900" 
runApplication -a foamDictionary system/controlDict -entry endTime -set "100" 
runApplication -a foamDictionary system/controlDict -entry deltaT -set "0.5" 

runApplication -o foamListTimes -rm 
rm -fr ./postProcessing
runApplication -o offbeat 
cp ./postProcessing/probes/0/betaFraction ./betaFraction
cp ./postProcessing/probes/0/T ./T

# Set BC for Q = 0
runApplication -o foamDictionary 0/T -entry internalField -set "uniform 900" 
runApplication -a foamDictionary 0/T -entry boundaryField/cladOuter/temperatureSeries/values -set "((0 900) (1e6 1400))" 
runApplication -a foamDictionary 0/T -entry boundaryField/cladOuter/value -set "uniform 900" 
runApplication -a foamDictionary system/controlDict -entry endTime -set "1e6" 
runApplication -a foamDictionary system/controlDict -entry deltaT -set "1e3" 

runApplication -o foamListTimes -rm 
rm -fr ./postProcessing
runApplication -o offbeat  
cp ./postProcessing/probes/0/betaFraction ./betaFractionEq
cp ./postProcessing/probes/0/T ./TEq

python3 <<EOF

import numpy as np
import matplotlib.pyplot as plt

B = np.genfromtxt('betaFraction', skip_header=2)
T = np.genfromtxt('T'           , skip_header=2)

Be = np.genfromtxt('betaFractionEq', skip_header=2)
Te = np.genfromtxt('TEq'           , skip_header=2)

Meq = np.genfromtxt('modelEq'   	 , skip_header=0)
Mpm = np.genfromtxt('modelPlusMinus' , skip_header=0)

# Print out value for verification
idx = np.where((Te[:,1] == 1150))
relErr = (float(Be[idx,1]) - 0.401681)/0.401681
with open("results", "w") as f:
	f.write("Eq. beta fraction at 1150 K (computed): {:f}".format(float(Be[idx,1])))
	f.write("\n")
	f.write("Eq. beta fraction at 1150 K (expected): 0.401681")
	f.write("\n")
	f.write("Relative error: {:f}\n".format(relErr))

with open("results", "a") as f:
	if ( relErr < 0.005 ):
		f.write("Test passed")
	else:
		f.write("Test failed")

plt.figure()

plt.plot(Meq[:,0], Meq[:,1], label='Massih model', marker='o', ls='', mfc='none', color ='k')
plt.plot(Mpm[:,0], Mpm[:,1], marker='o', ls='', mfc='none', color ='k')

plt.plot(Te[:,1], Be[:,1], label='OFFBEAT, eq', lw = 2, ls='-')
plt.plot(T[:,1], B[:,1], label='OFFBEAT, +-10 K/s', lw = 2, ls='--')

plt.xlim(1000,1300)
plt.ylim(0,1)

plt.grid(ls=':')

plt.xlabel("Temperature (K)")
plt.ylabel("Beta phase fraction (-)")

plt.legend()

plt.title("Zircaloy phase transition model, w=0 ")

plt.savefig("verificationBetaModel.png")
#plt.show()
EOF

rm T*
rm betaFraction*