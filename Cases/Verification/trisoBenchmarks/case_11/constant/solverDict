/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//- Thermal and Mechanical solver selection:
thermalSolver           solidConduction;
mechanicsSolver         smallStrainIncrementalUpdated;
neutronicsSolver        fromLatestTime;
elementTransport        fromLatestTime;

//- Material and rhelogy treatment:
materialProperties      byZone;
rheology                byMaterial;

heatSource              timeDependentVhgr;
burnup                  fromPower;
fastFlux                timeDependentAxialProfile;
corrosion               fromLatestTime;
gapGas                  TRISO;
fgr                     SCIANTIX;
sliceMapper             autoAxialSlices;

globalOptions
{
    pinDirection            (0 0 1);
    reactorType             "LWR";
}

thermalSolverOptions
{
    heatFluxSummary     off;
}

gapGasOptions
{
    gapPatches ( bufferOuter ipycInner );
    bufferOuterPatches bufferOuter;
    COProductionModel  Proksch;
    bufferPorosity  0.4681;
    gapVolumeOffset 0.0;
    gasReserveVolume 0.0;
    gasReserveTemperature 290;
}

heatSourceOptions
{
    timePoints  (0.0    1e9);
    vhgr        (3561803753.40845 3561803753.40845);
    // fission_rate * energy_per_fission
    // = 1.11306367294014E+020 f/m³/s * 3.2e-11 J/f
    materials ( Kernel );
}

rheologyOptions
{
    thermalExpansion on;
}

mechanicsSolverOptions
{
    // forceSummary        off;
    // cylindricalStress   on;
    //
     multiMaterialCorrection
     {
         type                    uniform;
        defaultWeights          1;
     }
    forceSummary        off;
    cylindricalStress   on;
    sphericalStress     on;
}

fastFluxOptions
{
    timePoints  ( 0     1e9);
    //- Derived by 7.2e25 n/m2 of 351 days
    fastFlux    (2.37416904083571e14  2.37416904083571e14); // n/cm2/s
    timeInterpolationMethod linear;

    axialProfile
    {
        type flat;
    }

    materials (Kernel Buffer IPyC SiC OPyC);
}


fgrOptions
{
    nFrequency  1;
    relax       1;
}


materials
{

  Kernel
  {
    material UO2;
    densityModel            UO2Constant;
    heatCapacityModel       UO2MATPRO;
    conductivityModel       UO2MATPRO;
    emissivityModel         constant;
    YoungModulusModel       constant;
    PoissonRatioModel       constant;
    thermalExpansionModel   constant;
    //
    densificationModel          none;
    swellingModel               none;
    relocationModel             none;


    emissivity emissivity [0 0 0 0 0]   0.0;
    // a very small value to avoid any mechnical interaction with the layers
    E           E       [1 -1 -2 0 0]   2e8;
    nu          nu      [0 0 0 0 0]     0.345;
    alpha       alpha   [0 0 0 0 0]     10e-6;
    Tref        Tref    [0 0 0 1 0]     1608;

      // //
      enrichment                  0.0982;
      rGrain                      5e-6;
      // GdContent                   0.0;
      theoreticalDensity          10960;
      densityFraction             0.9863; //10810.0
      dishFraction                0.0;
      //
      //
      isotropicCracking           off;

      rheologyModel               elasticity;
  }

  Buffer
  {
    material constant;

    rho         rho     [1 -3 0 0 0]    1000.0;
    Cp          Cp      [0 2 -2 -1 0]   720.0;
    k           k       [1 1 3 -1 0]    0.5;
    emissivity emissivity [0 0 0 0 0]   1.0;
    // a very small value to avoid any mechnical interaction with the layers
    E           E       [1 -1 -2 0 0]   2e8;
    nu          nu      [0 0 0 0 0]     0.345;
    alpha       alpha   [0 0 0 0 0]     5.5e-6;
    Tref        Tref    [0 0 0 1 0]     1608;

    rheologyModel               elasticity;
  }

    "IPyC|OPyC"
    {
      material PyC;

      densityModel                constant;
      conductivityModel           constant;
      heatCapacityModel           constant;
      emissivityModel             constant;
      YoungModulusModel           constant;
      PoissonRatioModel           constant;
      swellingModel               PyCCorrelation;
      radialCoefficients          (-1.43234e-1 2.62692e-1 -1.74247e-1 5.67549e-2 -8.36313e-3 4.52013e-4);
      tangentialCoefficients      (-3.24737e-2 9.07826e-3 -2.10029e-3 1.30457e-4 0.0 0.0);


      rho         rho     [1 -3 0 0 0]    1880.0;
      Cp          Cp      [0 2 -2 -1 0]   720.0;
      k           k       [1 1 3 -1 0]    4.0;
      emissivity emissivity [0 0 0 0 0]   1.0;
      E           E       [1 -1 -2 0 0]   3.96e10;
      nu          nu      [0 0 0 0 0]     0.33;
      alpha       alpha   [0 0 0 0 0]     5.5e-6;
      Tref        Tref    [0 0 0 1 0]     1608;

      rheologyModel misesPlasticCreep;
      rheologyModelOptions
      {
          plasticStrainVsYieldStress table
          (
              (0    1e60)
          );
          creepCoefficient         4.93e-4;
          poissonRationForCreep    0.4;
          relax 1;
          creepModel constantCreepPrincipalStress;
          fluxConversionFactor       1.0;
      }
    }

    SiC
    {
      material constant;

      rho         rho     [1 -3 0 0 0]    3200.0;
      Cp          Cp      [0 2 -2 -1 0]   620.0;
      k           k       [1 1 3 -1 0]    13.9;
      emissivity emissivity [0 0 0 0 0]   0.0;
      E           E       [1 -1 -2 0 0]   3.7e11;
      nu          nu      [0 0 0 0 0]     0.13;
      alpha       alpha   [0 0 0 0 0]     4.9e-6;
      Tref        Tref    [0 0 0 1 0]     1608;

      rheologyModel               elasticity;
    }

}

// ************************************************************************* //
