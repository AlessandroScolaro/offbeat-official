/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//- Thermal and Mechanical solver selection:
thermalSolver           solidConduction;
mechanicsSolver         smallStrainIncrementalUpdated;
neutronicsSolver        fromLatestTime;
elementTransport        fromLatestTime;

//- Material and rhelogy treatment:
materialProperties      byZone;
rheology                byMaterial;

heatSource              fromLatestTime;
burnup                  fromLatestTime;
fastFlux                timeDependentAxialProfile;
corrosion               fromLatestTime;
gapGas                  none;
fgr                     none;
sliceMapper             autoAxialSlices;
corrosion               fromLatestTime;

globalOptions
{
    pinDirection            (0 0 1);
    reactorType             "LWR";
}

thermalSolverOptions
{
    heatFluxSummary     off;
}

rheologyOptions
{
    thermalExpansion on;
}

mechanicsSolverOptions
{
    forceSummary        off;
    cylindricalStress   on;

    multiMaterialCorrection
    {
        type                    uniform;
        defaultWeights          1;
    }
}

fastFluxOptions
{
    timePoints  ( 0     10e7);
    //- change this value so that the object can have the different fluence at 10s.
    //- To verify the variation of E with fluence.
    fastFlux    (3.4722e13  3.4722e13); // n/cm2/s
    timeInterpolationMethod linear;

    axialProfile
    {
        type flat;
    }

    materials (IPyC SiC OPyC);
}

materials
{
    IPyC
    {
      material PyC;
      Tref                        Tref [ 0 0 0 1 0 ] 873.0;
      densityModel                constant;
      conductivityModel           constant;
      heatCapacityModel           constant;
      emissivityModel             constant;
      YoungModulusModel           constant;
      PoissonRatioModel           constant;
      thermalExpansionModel       constant;
      swellingModel               PyCCorrelation;
      radialCoefficients          (-1.80613e-2 9.82884e-3 -2.25937e-3 4.03266e-4 0.0 0.0);
      tangentialCoefficients      (-1.78392e-2 1.71315e-3 2.32979e-3 -4.91648e-4 0.0 0.0);


      rho         rho     [1 -3 0 0 0]    1900.0;
      Cp          Cp      [0 2 -2 -1 0]   1;
      k           k       [1 1 3 -1 0]    1;
      emissivity emissivity [0 0 0 0 0]   0.0;
      E           E       [1 -1 -2 0 0]   3.96e10;
      nu          nu      [0 0 0 0 0]     0.33;
      alpha       alpha   [0 0 0 0 0]     5.35e-6;

      rheologyModel misesPlasticCreep;

      rheologyModelOptions
      {
          plasticStrainVsYieldStress table
          (
              (0    1e60)
          );
          coefficientList            (4.386e-4 -9.70e-7 8.0294e-10);
          relax 1;
          creepModel correlationCreepPrincipalStress;
          fluxConversionFactor       1.0;
      }
    }

    SiC
    {
      material constant;

      rho         rho     [1 -3 0 0 0]    3200.0;
      Cp          Cp      [0 2 -2 -1 0]   1;
      k           k       [1 1 3 -1 0]    1;
      emissivity emissivity [0 0 0 0 0]   0.0;
      E           E       [1 -1 -2 0 0]   3.7e11;
      nu          nu      [0 0 0 0 0]     0.13;
      alpha       alpha   [0 0 0 0 0]     4.9e-6;
      Tref        Tref    [0 0 0 1 0]     873.0;

      rheologyModel               elasticity;
    }


    OPyC
    {
      material PyC;
      Tref                        Tref [ 0 0 0 1 0 ] 873.0;

      densityModel                constant;
      conductivityModel           constant;
      heatCapacityModel           constant;
      emissivityModel             constant;
      YoungModulusModel           constant;
      PoissonRatioModel           constant;
      thermalExpansionModel       constant;
      swellingModel               PyCCorrelation;
      radialCoefficients          (-1.80613e-2 9.82884e-3 -2.25937e-3 4.03266e-4 0.0 0.0);
      tangentialCoefficients      (-1.78392e-2 1.71315e-3 2.32979e-3 -4.91648e-4 0.0 0.0);


      rho         rho     [1 -3 0 0 0]    1900.0;
      Cp          Cp      [0 2 -2 -1 0]   1;
      k           k       [1 1 3 -1 0]    1;
      emissivity emissivity [0 0 0 0 0]   0.0;
      E           E       [1 -1 -2 0 0]   3.96e10;
      nu          nu      [0 0 0 0 0]     0.33;
      alpha       alpha   [0 0 0 0 0]     5.35e-6;

      rheologyModel misesPlasticCreep;

      rheologyModelOptions
      {
          plasticStrainVsYieldStress table
          (
              (0    1e60)
          );
          coefficientList            (4.386e-4 -9.70e-7 8.0294e-10);
          relax 1;
          creepModel correlationCreepPrincipalStress;
          fluxConversionFactor       1.0;
      }
    }
}

// ************************************************************************* //
