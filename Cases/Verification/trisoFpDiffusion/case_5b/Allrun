#!/bin/sh
# Run from this directory
cd "${0%/*}" || exit 1

# Source tutorial run functions
. "$WM_PROJECT_DIR/bin/tools/RunFunctions"

runApplication -o blockMesh
runApplication -o changeDictionary
runApplication -o offbeat

CsProd1=$(grep -i "Production of Cs" log.offbeat | tail -n 1| cut -d" " -f4)
CsRele1=$(grep -i "Released Cs amount" log.offbeat | tail -n 1| cut -d" " -f4)
AgProd1=$(grep -i "Production of Ag" log.offbeat | tail -n 1| cut -d" " -f4)
AgRele1=$(grep -i "Released Ag amount" log.offbeat | tail -n 1| cut -d" " -f4)

################################## 2nd simulation ############################
controlDict='./system/controlDict'
runApplication -o foamDictionary $controlDict -entry endTime -set 8.712e7
runApplication -o foamDictionary $controlDict -entry deltaT -set 3600
#
runApplication -o offbeat

CsProd2=$(grep -i "Production of Cs" log.offbeat | tail -n 1| cut -d" " -f4)
CsRele2=$(grep -i "Released Cs amount" log.offbeat | tail -n 1| cut -d" " -f4)
AgProd2=$(grep -i "Production of Ag" log.offbeat | tail -n 1| cut -d" " -f4)
AgRele2=$(grep -i "Released Ag amount" log.offbeat | tail -n 1| cut -d" " -f4)

#
CsProd=$(awk -v var1="$CsProd1" \
             -v var2="$CsProd2" \
             'BEGIN {print var1+var2}')

CsRele=$(awk -v var1="$CsRele1" \
             -v var2="$CsRele2" \
             'BEGIN {print var1+var2}')

AgProd=$(awk -v var1="$AgProd1" \
             -v var2="$AgProd2" \
             'BEGIN {print var1+var2}')

AgRele=$(awk -v var1="$AgRele1" \
             -v var2="$AgRele2" \
             'BEGIN {print var1+var2}')

fractionReleaseCs=$(awk -v var1="$CsRele" \
                        -v var2="$CsProd" \
                        'BEGIN {print var1/var2}')

fractionReleaseAg=$(awk -v var1="$AgRele" \
                        -v var2="$AgProd" \
                        'BEGIN {print var1/var2}')

CsExpected=0.000315053
AgExpected=0.398419

#
echo "Released Cs fraction: $fractionReleaseCs" >> results
echo "Codes range: [3.07e-4, 1.22e-3]" >> results
echo "Expected value: $CsExpected" >> results

echo "Released Ag fraction: $fractionReleaseAg" >> results
echo "Codes range: [0.14, 0.54]" >> results
echo "Expected value: $AgExpected" >> results
#
relErrorCs=$(awk -v var1="$fractionReleaseCs" \
                 -v var2="$CsExpected" \
                 'BEGIN {print 100*(var1-var2)/var2}')

relErrorAg=$(awk -v var1="$fractionReleaseAg" \
                 -v var2="$AgExpected" \
                 'BEGIN {print 100*(var1-var2)/var2}')


echo "Relative change for Cs: $relErrorCs%" >> results
echo "Relative change for Ag: $relErrorAg%" >> results

runApplication -o foamDictionary $controlDict -entry endTime -set 8.64e7
runApplication -o foamDictionary $controlDict -entry deltaT -set 86400
