/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  2.3.0                                 |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      solverDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

//- Thermal and Mechanical solver selection:
thermalSolver           fromLatestTime;
mechanicsSolver         fromLatestTime;
neutronicsSolver        fromLatestTime;
elementTransport        byList;

//- Material and rhelogy treatment:
materialProperties      byZone;
rheology                byMaterial;

heatSource              fromLatestTime;
burnup                  fromLatestTime;
fastFlux                fromLatestTime;
corrosion               fromLatestTime;
gapGas                  none;
fgr                     none;
sliceMapper             autoAxialSlices;
corrosion               fromLatestTime;

elementTransportOptions
{
  solvers
  (
      FpDiffusion
  );
  FpDiffusionOptions
  {
    outerPatches   outer;
    intialFpConcentration
    {
      Cs       1;
    }
  }
}

globalOptions
{
    pinDirection            (0 0 1);
    reactorType             "LWR";
}

thermalSolverOptions
{
    heatFluxSummary     off;
}

rheologyOptions
{
    thermalExpansion off;
}

mechanicsSolverOptions
{
    forceSummary        off;
    cylindricalStress   on;

    multiMaterialCorrection
    {
        type                    uniform;
        defaultWeights          1;
    }
}


materials
{
  Kernel
  {
    material UO2;
    Tref                        Tref [ 0 0 0 1 0 ] 298.15;

    densificationModel          none;
    swellingModel               none;
    relocationModel             none;
    emissivityModel             constant;
    enrichment                  0.025;
    rGrain                      6e-6;
    theoreticalDensity          10960;
    densityFraction             0.95;
    dishFraction                0.0;
    isotropicCracking           off;
    nCracksMax                  12;

    emissivity  emissivity      [0 0 0 0 0]     0.0;

    rheologyModel               elasticity;

    initialCsConcentration      1.0;

    poreVelocityModel none;
  }
}

// ************************************************************************* //
