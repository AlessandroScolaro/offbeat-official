import os

# Path to the mdFiles folder
md_files_folder = '../../mdFiles'
# Path to the html folder
html_folder = '../html'

# Get a list of all .md files in the mdFiles folder
md_files = [f for f in os.listdir(md_files_folder) if f.endswith('.md')]

# Process each .md file
for md_file in md_files:
    
    # Extract the class name from the file name
    motherClass_name, class_name = os.path.splitext(md_file)[0].split('_')

    # Get the corresponding HTML file
    html_file = os.path.join(html_folder, 'classFoam_1_1' + class_name + '.html')

    # Check if the HTML file exists
    if os.path.isfile(html_file):

        # Read the HTML file
        with open(html_file, 'r') as f:
            html_content = f.readlines()

        # Find the starting index of the lines to remove
        start_index = None
        for i, line in enumerate(html_content):
            if line.strip() == '<p><a class="anchor" id="Description"></a></p>':
                start_index = i
                break     

        # Find the ending index of the lines to remove
        end_index = None
        for i, line in enumerate(html_content[start_index:]):
            if line.strip() == '</div><!-- PageDoc -->':
                try:
                    end_index = i + start_index - 1
                    break                                
                except:
                    continue                    

        # Find the <div class="title"> element and extract the text
        title_text = None                
        title_line_index = None
        for i, line in enumerate(html_content[start_index:]):
            if line.strip().startswith('<div class="title">'):
                title_text = line.strip()[len('<div class="title">'):-len('</div>')].strip()
                try:
                    title_line_index = i + start_index
                    break                                
                except:
                    continue  

        if not(title_line_index == None or start_index == None or end_index == None):
                                       

            # Remove 10 lines from the ending index
            if start_index is not None:
                del html_content[end_index : end_index+11]   

            # Create the new endline to insert
            endline_to_insert = f'</div>\n'

            # Insert the new endline at the appropriate position
            html_content.insert(end_index, endline_to_insert)                

            # Remove lines from the starting index until the title line
            if start_index is not None:
                del html_content[start_index+1 : title_line_index+3]                              

            # Create the new line to insert
            line_to_insert = f'<h3 id="{class_name}">{title_text}</h3>\n'

            # Insert the new line at the appropriate position
            html_content.insert(start_index+1 , line_to_insert)

            # Write the modified HTML content back to the file
            with open(html_file, 'w') as f:
                f.writelines(html_content)
