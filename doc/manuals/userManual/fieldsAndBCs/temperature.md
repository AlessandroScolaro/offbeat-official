# Temperature Boundary Conditions {#temperatureBCs}

This section summarizes the main thermal boundary conditions currently available in OFFBEAT. All the available options are quickly visible using the "banana method", i.e. using a dummy keywords (e.g "banana") for the boundary condition type. Running the case OpenFOAM will detect the mistake and propose a set of possible valid alternatives to the dummy keyword.


### Fixed value (Dirichlet) boundary conditions

- [fixedTemperature](@ref Foam.fixedTemperatureFvPatchScalarField) Imposes a fixed temperature on the patch.
- [timeDependentTemperature](@ref Foam.timeDependentTemperatureFvPatchScalarField) Imposes a fixed temperature history on the patch.
- [axialProfileT](@ref Foam.axialProfileFvPatchScalarField) Imposes a fixed temperature axial profile on the patch.
- [timeDependentAxialProfileT](@ref Foam.timeDependentAxialProfileFvPatchScalarField) Imposes a time-dependent axial temperature profile on the patch.
- [xzTemperatureProfile](@ref Foam.xzTemperatureProfileFvPatchScalarField) Imposes a x-dependent axial temperature profile on the patch.


### Coupled Boundary conditions
BCs developed for treating (primarily) the heat exchange between fuel and cladding across the gap. Some of them can be be used also to treat the heat conduction between stacked fuel pellets.

| Boundary Condition Name  | Brief Description                                                                                            | Class                                  |
|--------------------------|--------------------------------------------------------------------------------------------------------------|----------------------------------------|
| `temperatureCoupled`     | BC that forces continuity (for the temperature field) between two bodies with different material properties. | temperatureCoupledFvPatchScalarField.H |
| `resistiveGap`           | BC that simulate the presence of a fixed gap (or contact) resistance between two bodies.                     | resistiveGapFvPatchScalarField.H       |
| `fuelRodGap`             | BC that computes the gap conductance as sum of contact, radiative and gas conductive contributions.          | fuelRodGapFvPatchScalarField.H         |


### Heat exchange (Neumann) boundary conditions

- [convectiveHTC](@ref Foam.convectiveHTCFvPatchScalarField) It models the heat exchange with a heat-sink, using a fixed temperature and heat exchange coefficient.
- [timeDependentAxialProfileHTC](@ref Foam.timeDependentAxialProfileHTCfvPatchScalarField) It models the heat exchange with a heat-sink, using a time- and axial-dependent fluid temperature and heat exchange coefficient.
- [radiativeConvectiveSink](@ref Foam.radiativeConvectiveSinkFvPatchScalarField) Same as `convectiveHTC`, but with the addition of radiative heat transfer.   


