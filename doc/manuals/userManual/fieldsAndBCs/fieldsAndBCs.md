# Fields and Boundary Conditions {#fieldsAndBCs}

For general instructions on using fvPatchField classes in OpenFOAM, please refer to (depending on your OpenFOAM version) the [OpenFOAM.org user guide](https://doc.cfd.direct/openfoam/user-guide-v10/) or the [OpenFOAM.com user guide](https://www.openfoam.org/documentation/user-guide).


OFFBEAT boundary conditions are specified similarly to standard OpenFOAM cases. Temperature and Displacement fields at the initial configuration need to be specified for each domain boundary patch. The `0/` folder of the case contains a dictionary file for both fields, where the conditions are set according to the `type` keyword specified for each patch under the `boundaryField` section.

Among the available boundary conditions the user can choose from the standard OpenFOAM conditions (i.e. `fixedValue`, `zeroGradient`, etc.) and conditions specifically developed in OFFBEAT to meet fuel performance requirements. An overview of the latter is given in the following sub-sections:

* [Temperature BCs](@ref temperatureBCs)
* [Displacement BCs](@ref displacementBCs)

If necessary, the user can set initial and boundary conditions for other fields such as burnup or swelling in a similar manner, that is by creating an appropriate file with the `internalField` and `boundaryField` properly set. Indeed, this is how the code is able to restart a simulation by reading the field files from the latest available time step. Setting additional fields, however, is not required (nor suggested for most cases) to start a new simulation.

Additionally, the user must set the gap gas composition and initial pressure in the *gapGas* file, always inside the starting time folder (see [Gap Gas model](@ref gapGas)).

***

Return to [User Manual](@ref userManual)
