# General Instructions {#general}

OFFBEAT operation is similar to typical solvers shipped with OpenFOAM (e.g. icoFoam, pisoFoam, etc.) in that the user provides the code with a mesh, a control dictionary `controlDict`, a solution parameters dictionary `fvSolution`, a discretization schemes dictionary `fvSchemes`, an OFFBEAT-specific solver dictionary `solverDict`, as well as initial and boundary conditions for the main fields in the initial time step folder (e.g. the folder `0/`).

For a more comprehensive understanding of OFFBEAT's structure and workflow, the following sections provide in-depth insights:

* [Folder Structure](@ref folderStructure)
* [Computational Domain, Mesh and Boundary Definitions](@ref computationalDomain)
* [Workflow](@ref workflow)

***

Return to [User Manual](@ref userManual)
