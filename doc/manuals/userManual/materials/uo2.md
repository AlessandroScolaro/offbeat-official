# UO2 material model {#uo2}

Material model for UO2 fuel material (see UO2.H). The model uses default selections for thermomechanical properties and behavioral models, but different choices can be selected by the user. 

The default selections of the material model are listed in the table below.

Please refer to the header of each listed file for more information on the specific correlation/model.

### Thermomechanical properties

| Thermomechanical property | Default model                 |
|---------------------------|-------------------------------|
| Thermal Conductivity      | conductivityMatproUO2.H       |
| Density                   | constantDensityUO2.H          |
| Emissivity                | emissivityRelapUO2.H          |
| Heat Capacity             | heatCapacityMatproUO2.H       |
| Poisson's Ratio           | constantPoissonRatioUO2.H     |
| Thermal Expansion         | thermalExpansionRelapUO2.H    |
| Young's Modulus           | YoungModulusMatproUO2.H       |

### Behavioral models

| Behavioral phenomenon     | Default model                 |
|---------------------------|-------------------------------|
| Relocation                | relocationFRAPCON.H           |
| Densification             | densificationFRAPCON.H        |
| Swelling                  | swellingFRAPCON.H             |
| Failure                   | failureModel.H (i.e. "none")  |
