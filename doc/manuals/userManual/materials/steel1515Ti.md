# Stainless Steel (15-15 Ti) material model {#steel1515Ti}

Material model for Stainless Steel (15-15 Ti) cladding material (see steel1515Ti.H). The model uses default selections for thermomechanical properties and behavioral models, but different choices can be selected by the user. 

The default selections of the material model are listed in the table below.

Please refer to the header of each listed file for more information on the specific correlation/model.

### Thermomechanical properties

| Thermomechanical property | Default model                 |
|---------------------------|-------------------------------|
| Thermal Conductivity      | conductivityTobbe1515Ti.H     |
| Density                   | densitySchumann1515Ti.H       |
| Emissivity                | -                             |
| Heat Capacity             | heatCapacityBanerjee1515Ti.H  |
| Poisson's Ratio           | PoissonRatioTobbe1515Ti.H     |
| Thermal Expansion         | thermalExpansionGehr1515Ti.H  |
| Young's Modulus           | YoungModulusTobbe1515Ti.H     |

### Behavioral models

In absence of suitable correlations, default behavioral models for (U,Pu)O2 correspond to the ones for UO2 fuel material.

| Behavioral phenomenon     | Default model                      |
|---------------------------|------------------------------------|
| Swelling                  | swellingGrowthGeneralized1515Ti.H  |
| Failure                   | failureModel.H (i.e. "none")       |