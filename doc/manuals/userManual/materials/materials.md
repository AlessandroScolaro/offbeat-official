# Material models {#materials}

OFFBEAT is provided with several material models, each of which includes default correlations for the thermomechanical properties of the material and default behavioral models for the main phenomena of interest for the material.

The available material models of OFFBEAT are listed hereafter:

* [Constant material](@ref constantMaterial)
* [UO2](@ref uo2)
* [Zircaloy](@ref zircaloy)
* [UPuO2](@ref upuo2)
* [Molybdenum](@ref molybdenum)
* [Stainless Steel (15/15Ti)](@ref steel1515Ti)
* [Inconel 600](@ref inconel600)

***

Return to [User Manual](@ref userManual)
