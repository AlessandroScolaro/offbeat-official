# Mechanics Solution {#mechanics}

The linear momentum balance equation is the governing equation for mechanics in OFFBEAT, which in its general form is described by the following equation:
\f[
		\int_V{\frac{\partial^2\rho D}{\partial t^2}dV} = \oint_S{n\cdot\sigma dS} + \int_V{\rho b dV}
\f]
where:
* \f$ V \f$ is the body volume
* \f$ S \f$ is the surface of the volume V
* \f$ n \f$ is the unit vector pointing outward the surface S
* \f$ \rho \f$ is the density
* \f$ D \f$ is the displacement vector
* \f$ \sigma \f$ is the Cauchy stress tensor
* \f$ b \f$ is a generic body force per unit mass

The equation above states that the sum of all the forces
applied to the body is equal to the rate of change of its total linear momentum or its inertia.

The different mechanical solvers available in OFFBEAT are listed below:
- By selecting the mother class mechanicsSubSolver.H with the keyword fromLatestTime the displacement field "D" is read from latest time folder and no equation is solved for. If the file is not present, the displacement is set to (0 0 0) by default with zero-gradient BCs.
- Small Strain solver (see smallStrain.H) : solves the momentum continuity equation with small-strain approximation. It solves for total displacement D and does not update the mesh.
- Small Strain Updated solver (see smallStrainIncUpdated.H) : solves the momentum continuity equation with small-strain approximation but updating computational mesh. It solves for incremental displacement DD.
- Large Strain Total-Lagrangian solver (see largeStrainTotLag.H) : solves the momentum continuity equation with large-strain approach but not updating the computational mesh. It solves for total displacement D.
- Large Strain Updated-Lagrangian solver (see largeStrainUpdLag.H) : solves the momentum continuity equation with large-strain approach updating the computational mesh. It solves for incremental displacement DD.

***

Return to [Setting the `solverDict`](@ref solverDict)
