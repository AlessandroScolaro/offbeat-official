# Burnup model {#burnup}

The <code><b>burnup</b></code> class is used to enable the modeling of burnup evolution in OFFBEAT.
The burnup field is by default named <code><b>Bu</b></code> and is in <code><b>(MWd/MT<sub>oxide</sub>)</b></code>. 

### Usage
The burnup model <u>must</u> be selected with the <code><b>burnup</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code>  dictionary, located in the <code><b>constant</b></code>  folder). 

Currently OFFBEAT supports the following burnup models:

- [none](@ref Foam.burnup) - neglects burnup evolution, the burnup field is not created;
- [fromLatestTime](@ref Foam.constantBurnup) - the burnup field  is read from the <code><b>Bu</b></code> file in the starting time folder;
- [fromPower](@ref Foam.burnupFromPower) - the burnup field is automatically derived from the local heat source;
- [Lassmann](@ref Foam.burnupLassmann) - employs a simplified depletion module that solves the Bateman equations for a small set of relevant nuclides with the objective of calculating the power density radial form factor;

***

Return to [User Manual](@ref userManual)