# 3-D to 1-D Mapping {#mapper}

Some behavioral models such as the correlations used for calculating the relocation strains or the TUBRNP burnup module are derived from traditional 1.5-D codes. For this reason, they might require the average value of a given quantity (e.g. the burnup) calculated over a 1-D radial slice.

The same is true when running OFFBEAT in 1.5-D mode with the `modifiedPlaneStrain` option activated: the axial force balance necessary to obtain the axial strain component is calculated slice-by-slice.

The concept of axial slices is not native in OpenFOAM and cannot be generalised to arbitrary 3D domains using unstructured meshes. For this reason, OFFBEAT includes a slice mapper class in which the user specifies the method used to define 1-D axial slices superimposed on the 3-D mesh.

The mapper type is selected with the `mapper` keyword in the *solverDict* dictionary.
Currently OFFBEAT supports the following mapper models:
- By selecting the mother class sliceMapper.H with the keyword `none` no mapper is considered. Errors might occur if models are activated that require the presence of a mapper.
- sliceMapperByMaterial.H, the slices are defined for each material inside the respective dictionary inside the `materials` subdictionary.
- sliceMapperByPellets.H, slices are defined pellet-wise by homogeneously dividing the total fuel stack height by a user-defined pellet height
- sliceMapperAutoAxialSlices.H, automatic slices are created for every material according to the axial quote of each mesh cell. All the cells belonging to the same material and having the same axial quote are considered part of the same slice.

***

Return to [Setting the `solverDict`](@ref solverDict)
