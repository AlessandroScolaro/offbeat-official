# Neutronics Solution {#neutronics}

The <code><b>neutronicsSubSolver</b></code> class is used to solve for the neutron flux distribution within the fuel pellets. This feature might be necessary to capture the radial power peaking profile within the fuel.

### Usage
The neutronics solver <u>must</u> be selected with the <code><b>neutronicsSolver</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code>  dictionary, located in the <code><b>constant</b></code>  folder). 

Currently OFFBEAT supports the following neutronics solvers:

- [fromLatestTime](@ref Foam.neutronicsSubSolver) - deactivates the neutronics solution (i.e. no equation is solved). No additional field is created;
- [diffusion](@ref Foam.diffusionSolver) - it solves the one-group neutron diffusion equation to obtain the thermal intra-pellet thermal neutron flux profile.

***

Return to [User Manual](@ref userManual)