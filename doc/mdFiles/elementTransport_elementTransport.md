### User documentation for `fromLatestTime` element transport {#elementTransport_elementTransport}

For general instructions on the modeling of element transport in OFFBEAT see [here](elementTransport.html).<br><br>

By selecting the `fromLatestTime` option for the elementTransport solver, the user can disable all the transport solvers (e.g. porosity, Plutonium, Americium, Hydrogen) available in OFFBEAT. In this way, no additional equation is solved for these fields. To use the `fromLatestTime` element transport option in OFFBEAT, the user will need to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    elementTransport fromLatestTime;
    
</code></pre>
</div>	
