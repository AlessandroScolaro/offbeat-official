### User documentation for `misesPlasticCreep` class {#constitutiveLaw_misesPlasticCreep}

For general instructions on the modeling of the rheology of a material in OFFBEAT see [here](rheology.html).\\
For more informations on the J2/Mises plasticity, see [here](constituveLaw_misesPlasticity.html).

The `misesPlasticCreep` constitutive law class implements the linear elastic-misesPlasticity-creep mechanical behaviour law.\\
Conceptually, this class add a creep component on top of the plastic strain component to the total strain tensor.
See the [`creepModel`](creepModel_creepModel.html) for a list of available creep models in OFFBEAT.

The stress follow a Hooke linear elastic mechanical law:
\f[
    \underline{\sigma} = 2\mu\times \underline{\varepsilon} + \lambda \times tr(\underline{\varepsilon})
\f]
where 
- \f$\underline{\sigma}\f$ is the Cauchy stress tensor
- \f$\mu\f$ and \f$\lambda\f$ are the lamé parameters
- \f$\underline{\varepsilon}\f$  is the small strain tensor

This law assumes that the strain tensor components are additive:
\f[
    \varepsilon = \varepsilon^{el} + \varepsilon^{creep} + \varepsilon^{plastic} etc..
\f]

To calculate the stresses, the creep and plastic strain components are subtracted from the total strain tensor. 



#### Usage  

To use the `misesPlasticCreep` constitutive law in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
rheology byMaterial;
...
materials
{
    ...
    cladding
    {
        material zircaloy;
        ...
        rheologyModel misesPlasticCreep;
        rheologyModelOptions
        {
            yieldStressModel    plasticStrainVsYieldStress;
            plasticStrainVsYieldStress table
            (
                (0    200e6)
            );
            creepModel LimbackCreepModel;
        }
    }
}
</code></pre>
</div>	