### User documentation for `misesPlasticity` class {#constitutiveLaw_misesPlasticity}

For general instructions on the modeling of the rheology of a material in OFFBEAT see [here](rheology.html).

The `misesPlasticity` constitutive law class implements the classical Von Mises theory of plasticity.
It makes use of the Von Mises stress criterion:
$$
	\sigma_{eq} = \sqrt{\frac{3}{2}\underline{s}:\underline{s}}=\sqrt{3J_2}
$$
where:
- \\(\underline{s} \\) is the deviatoric stress defined as:
$$
	\underline{s}=\underline{\sigma} - \frac{1}{3}tr(\underline{\sigma})\underline{\textbf{I}}
$$
\\(J_2 \\) being the second invariant of \\(\underline{s} \\).

If we consider the eigenvalues of the stress tensor, the Von Mises equivalent stress can also be expressed as:
$$
	\sigma_{eq} = \sqrt{\frac{1}{2} (|\sigma_1 - \sigma_2|^2 + |\sigma_1 - \sigma_3|^2 + |\sigma_2 - \sigma_3|^2)}
$$

The plastic flow is defined by:
- a yield surface \\(f \\)
- a plastic potential \\(g \\)

The plastic strain rate satisfies:
\f[
	\underline{\dot{\varepsilon}}^{p} = \dot{\lambda} \frac{\partial g}{\partial \underline{\sigma}}
\f]


The plastic multiplier \f$\lambda\f$ classicaly satisfies the Kuhn-Tucker condition:
\f[
	\dot{\lambda}  f (\underline{\sigma}, p) = 0
\f]
\f[
	\dot{\lambda} \geq 0
\f]


The plastic flow being associative, \f$ f \f$is equal to the plastic potential.
In its most general form, the yield surface is defined by a stress criterion \f$\phi\f$, a set of kinematic hardening rules \f$\underline{X}\f$ and an a set of isotropic hardening rules \f$R(p)\f$:
\f[
	f(\underline{\sigma}, p) = \phi(\underline{\sigma}_{eq} - \underline{X}) - R(p)
\f]
with \f$p\f$ the equivalent plastic strain.

To date, kinematic hardening (i.e, the displacement of the yield surface in the stress space) is not implemented. Only isotropic hardening (i.e, expansion or shrinkage of the yield surface) is implemented.

#### Usage  

To use the `misesPlasticity` constitutive law in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
rheology byMaterial;
...
materials
{
    ...
    cladding
    {
        material zircaloy;
        ...
        rheologyModel misesPlasticity;
        rheologyModelOptions
        {
        	...
        }
    }

}
</code></pre>
</div>	

The `rheologyModelOptions` subDictionnary requires the user to specifiy the `yieldStressModel` as:
- a constant (no hardening):
<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
...
rheologyModel misesPlasticity;
rheologyModelOptions
{
    yieldStressModel constant;
    sigmaY 190e9;
}
</code></pre>
</div>	

- a predefined yieldStressModel implemented in OFFBEAT (hardening, strain rate dependancy or any other revelant state variable):
<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
...
rheologyModel misesPlasticity;
rheologyModelOptions
    {
    	yieldStressModel yieldStressFRAPTRAN; //- Yield stress model of FRAPTRAN for Zyrcaloy 4 under RIA transient. 
    										  //	  Function of temperature, strain rate, fast neutron fluence and cold work amount.
    	phi     0.0; //Fast neutron (E > 1 MeV) fluence in 1e25 n/m2, up to 12.
    	CW      0.0; //Cold work amount (0 to 0.75 range)
    }

</code></pre>
</div>  

- a plasticStrainVsYieldStress table, describing the evolution of the yield stress with increasing plastic strain (linear or non-linear hardening). 
<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
...
rheologyModel misesPlasticity;
rheologyModelOptions
{
	yieldStressModel    plasticStrainVsYieldStress;
	plasticStrainVsYieldStress table
    (
        (0.0 200e6)
        (0.005 250e6)
        (0.01 300e6)
    );
}
</code></pre>
</div>  
The interpolation between two points is linear. User should carefully discretize the evolution of the yield stress for each simulation. 
<!-- </details> -->
