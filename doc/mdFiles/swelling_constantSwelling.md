### User documentation for `constantSwelling` class {#swelling_constantSwelling}

Class caculating the swelling with a provided constant swelling rate.
This class is used to verify the OFFBEAT results with IAEA CPR-6 benchmark.

#### Formulation

The irradiation-induced eigenstrain of the material is given by the correlation below:

$$
\begin{aligned}
\varepsilon = C\phi
\end{aligned}
$$

where:

- \\(\varepsilon \\) is irradiation-induced eigenstrain,
- \\(C \\) is the user-provided constant,
- \\(\phi \\) is fast neutron fluence(\\(10^{25}n/m^2 \\), E>0.18 MeV).

#### Usage

To use `constantSwelling` in OFFBEAT, you need to specify the keyword **constant** for `swellingModel` and provide the `swellingRate`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        PyC
        {
            material PyC;
            swellingModel       constant;
            swellingRate        -0.005;
        }

        ...

    }

</code></pre>
</div>

#### Input parameters

|                |   Description                   |     Default            |
|----------------|---------------------------------|------------------------|
|swellingRate     |Swelling rate. In unit of /(10^25n/m²)  |**Required**      |
|fastFluenceName  |Name of fast fluence field        |fastFluence |
