### User documentation for `TobbeDINCreepModel` class {#creepModel_TobbeDINCreepModel}

#### Description
The `TobbeDINCreepModel` implements the computation of the thermal and irradiation creep strain rate for the 1.4970m specific steel of the 15-15Ti class for fast reactors.

The thermal creep strain rate (in \f$\%\mathrm{h}^{-1}\f$) is given as:
\f[
    \dot{\varepsilon}^{th} = 7.49\times 10^{15} \mathrm{exp}(-\frac{91000}{1.986T}) \mathrm{sinh}( \frac{57.46\sigma}{1.986 T})
\f]
wherE:
- T is the temperature in Kelvin
- \\(\sigma \\) is the effective stresss (Von Mises) in MPa

The irradiation creep rate is given (in \f$\%\cdot\mathrm{h}^{-1}\f$) as:
\f[
    \dot{\varepsilon}^{irr} = 7.49\times10^{}\overline{E}\varphi\sigma
\f]
where:

-\\(\overline{E} \\) is the average neutron energy. It is hardcoded to 1 MeV in OFFBEAT.
-\\(\varphi \\) is the total neutron flux (\f$\mathrm{n}\cdot\mathrm{cm}^{-2}\cdot\mathrm{s}^{-1}\f$). In OFFBEAT, only the fast flux is considered.
-\\(\sigma \\) is the effective stress (Von Mises) in MPa.

The total creep rate (in \f$\%\cdot\mathrm{h}^{-1}\f$) is thus given as:
\f[
    \dot{\varepsilon}^{creep} = \dot{\varepsilon}^{th} + \dot{\varepsilon}^{irr}
\f]


#### Usage
To use the `TobbeDINCreepModel` creep model in OFFBEAT, you will need to specify it in the `solverDict` using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
rheology byMaterial;
...
materials
{
    ...
    cladding
    {
        material zircaloy;
        ...
        rheologyModel misesPlasticCreep;
        rheologyModelOptions
        {
            yieldStressModel constant;
            sigmaY 190e9;

            creepModel TobbeDINCreepModel;
        }
    }
}
</code></pre>
</div>	

#### References
L. Luzzi et al, Modeling and Analyasis of Nuclear Fuel Pin Behavior for Innovative Lead Cooled FBR, ENEA, report RdS/PAR2013/022, 2013

Többe, Das Brennstabrechenprogramm IAMBUS zur Auslegung von Schellbrüter. Brennstäben, Tech. Bericht ITB 75.65, 1975.