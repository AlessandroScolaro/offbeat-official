### User documentation for `heatCapacitySneadSiC` class {#heatCapacity_heatCapacitySneadSiC}

Correlation for SiC heat capacity from Snead et al

#### Formulation

$$
\begin{aligned}
Cp = 925.65+0.3772T-7.9259\cdot10^{-5}T^2-\frac{3.1974\cdot10^7}{T^2}
\end{aligned}
$$

where:

- \f$Cp\f$ is the specific heat(J/kg/K),
- \f$T\f$ is the temperature(K).



#### Usage

To use `heatCapacitySneadSiC` in OFFBEAT, you just need to specify the keyword **SiCSnead** for `conductivityModel`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        SiC
        {
            heatCapacityModel  SiCSnead;
        }

        ...

    }

</code></pre>
</div>
#### References

  -   L. L. Snead, T. Nozawa, Y. Katoh, T.-S. Byun, S. Kondo, and D. A. Petti. Handbook of sic properties for fuel performance modeling. Journal of Nuclear Materials, 371:329–377, 2007.
