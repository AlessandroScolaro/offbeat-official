### User documentation for `porosityTransport` solver {#transportSolver_porosityTransportSolver}

For general instructions on the modeling of element transport in OFFBEAT see [here](elementTransport.html). 

The `porosityTransport` transport solver solves an advection-diffusion equation to find the porosity redistribution within fuel pellets during irradiation.

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Formulation </summary> -->

#### Formulation

The advection-diffusion equation is the following:

$$
\begin{aligned} 
\frac{\partial p}{\partial t} + \nabla \cdot [p(1-p)\cdot v  - \nu\nabla p] = 0
\end{aligned}
$$

where:

- \\( p \\)  is the porosity,
- \\( v \\)  is the pore velocity,
-  \\( \nu \\) is a diffusion coefficient used to smooth out numerical oscillations.

The first factor of the advective term, i.e. \\( (1-p) \\) is used to bound the porosity solution in the range \\( [0,1] \\). An user selectable option allows to use the "unbounded" version of this equation, where the \\( (1-p) \\) term is disregarded (i.e. assumed = 1).

The advection and diffusion contributions can be switched off with the dedicated keywords.

<!-- </details> -->

<!-- <details> 
	    <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To use the `porosityTransport` solver in OFFBEAT, the user will need to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	elementTransport byList;

	elementTransportOptions
	{
		solvers        ( porosityTransport );
		
		porosityOptions
		{
			poreMigration off;
			advectiveTerm   true;           // True by default
			diffusiveTerm   true;           // True by default
			boundedPoreAdvectionTerm false; // True by default
		}
	}

</code></pre>
</div>
<!--  -->
<!-- </details> -->
		