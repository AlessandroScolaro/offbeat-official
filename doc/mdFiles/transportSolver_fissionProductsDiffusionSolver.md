### User documentation for `fissionProductsDiffusionSolver` solver {#transportSolver_fissionProductsDiffusionSolver}

For general instructions on the modeling of element transport in OFFBEAT see [here](elementTransport.html).

The `fissionProductsDiffusionSolver` solver solves for the migration of fission products (FP) such as Cs, Ag in TRISO fuel particles.

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Formulation </summary> -->

#### Formulation

The migration of fission products inside the TRISO layers are governed by the Fick’s law with the effective diffusion coefficient, which is modelled through the following PDE:

$$
\begin{aligned}
\frac{\partial C}{\partial t} + \nabla\cdot\left(-D\nabla C\right) + \lambda C - S = 0
\end{aligned}
$$

where:

- \\( C \\)  is the FP concentration (mol/m³),
- \\( t \\)  is the time (s),
- \\( D \\)  is the diffusion coefficient (m²/s),
- \\( lamda \\)  is the decay constant (/s),
- \\( S \\)  is the source term (mol/m³/s).

For species with extended half-lives, like Cs, Kr and Ag, the decay term can be neglect. The diffusion coefficient is calculated on the level of materials and can be found in [diffCoefModel](@ref Foam.diffCoefModel).

The source term can be obtained through the fission yields (atom/fission). The fission yields for cesium , silver and krypton and xenon can be found in the reference.

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To use the `PuRedistribution` solver in OFFBEAT, the `elementTransport`should be switch on to **byList**.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  elementTransport        byList;
</code></pre>
</div>

Next within `elementTransportOptions`, set the `solvers` to **FpDiffusion**. Inside the dictionary `FpDiffusionOptions`, you need to define 2 parameters.

* `outerPatches` - The name of the boundary patche. The default name is **outer**.
* `intialFpConcentration` - The initial Fp concentration within the Kernel. Pleases note the fields for FP inside this dictionary will be generated automatically.

If a heat source is provided, then the production of these FP will be calculated within the Kernel. Otherwise there will be no proudcion of FP. Currently, the production rate is available for Cs, Kr (Kr+Xe), Ag and Sr.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; overflow: scroll; height: 200px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
elementTransportOptions
{
  solvers
  (
      FpDiffusion
  );
  FpDiffusionOptions
  {
    outerPatches   outer;
    intialFpConcentration
    {
      Cs       0;
      Ag       0;
    }
  }
}
</code></pre>
</div>

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> References </summary> -->

#### References

  - Hales J D, Jiang W, Toptan A, et al. BISON TRISO Modeling Advancements and Validation to AGR-1 Data[R]. Idaho National Lab.(INL), Idaho Falls, ID (United States), 2020.

<!-- </details> -->
