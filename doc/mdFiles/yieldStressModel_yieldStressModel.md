### User documentation for `yieldStressModel` class {#yieldStressModel_yieldStressModel}

The `yieldStressModel` class is the mother class for all the types of yield stresses implemented in OFFBEAT.
It is intended to be used with the 'misesPlasticity' constitutive mechanical behaviour law.
