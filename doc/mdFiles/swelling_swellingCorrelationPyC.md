### User documentation for `swellingCorrelationPyC` class {#swelling_swellingCorrelationPyC}

Class handling irradiation-induced dimensional change eigenstrain phenomenon for PyC with the user-provided correlation for both radial and tangential directions.This class is used to verify the OFFBEAT results with IAEA CPR-6 benchmark.

#### Formulation

The irradiation-induced eigenstrain of the material is given by the correlation below:

$$
\begin{aligned}
\frac{d\varepsilon}{d\phi} = \sum_{i=0}^{5} A_i\phi^i
\end{aligned}
$$

where:

- \\(\varepsilon \\) is irradiation-induced eigenstrain,
- \\(A_i \\) is the user-provided constants,
- \\(\phi \\) is fast neutron fluence(\\(10^{25}n/m^2 \\), E>0.18 MeV).

#### Usage

To use `swellingCorrelationPyC` in OFFBEAT, you need to specify the keyword **PyCCorrelation** for `swellingModel` and provide the `radialCoefficients` and `tangentialCoefficients`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        IPyC
        {
            material PyC;

            ...

            swellingModel              PyCCorrelation;
            sphereCoordinate           true;
            radialCoefficients        (A1 A2 A3 A4 A5);
            tangentialCoefficients    (B1 B2 B3 B4 B5);
        }

        ...
    }

</code></pre>
</div>

#### Example

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    swellingModel               PyCCorrelation;
    radialCoefficients          (-1.43234e-1 2.62692e-1 -1.74247e-1 5.67549e-2 -8.36313e-3 4.52013e-4);
    tangentialCoefficients      (-3.24737e-2 9.07826e-3 -2.10029e-3 1.30457e-4 0.0 0.0);}

</code></pre>
</div>

#### Input parameters

|                |   Description                   |     Default            |
|----------------|---------------------------------|------------------------|
|sphereCoordinate  |Whether to use spherical coordinate(in 1D case) |true |
|asFabricatedAnisotropy |The as-fabricated anisotropy (dimensionless) |1.0 |
|fluxConversionFactor |Flux conversion factor. Conversion between flux(>0.1 MeV) to flux(>0.18 MeV) |1.0 |
|radialCoefficients |Radial coefficients | **Required** |
|tangentialCoefficients |Tangential Coefficients | **Required** |
