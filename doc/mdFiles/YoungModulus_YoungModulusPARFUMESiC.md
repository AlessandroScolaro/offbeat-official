### User documentation for `YoungModulusPARFUMESiC` class {#YoungModulus_YoungModulusPARFUMESiC}

Class modelling Young Modulus of SiC with the Interpolation from PARFUME.

#### Formulation

The elastic modulus of SiC is interpolated by the temperature with the table below. The temperature will keep as the bound value once it's below or above the bound.

|Temperature (\f$^\circ C\f$) |E (GPa) |
|-----------------------|--------|
|25.0 |428.0 |
|940.0 |375.0 |
|1215.0 |340.0 |
|1600.0 |198.0 |



#### Usage

To use `YoungModulusPARFUMESiC` in OFFBEAT, you just need to specify the keyword **SiCPARFUME** for `YoungModulusModel`.

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
    materials
    {
        SiC
        {
            YoungModulusModel  SiCPARFUME;
        }

        ...

    }

</code></pre>
</div>
