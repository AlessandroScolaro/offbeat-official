### User documentation for `fissionProductsDiffusionSolver` solver {#diffusionCoefficient_diffCoefModel}

Mother class for the diffusion coefficients of [fissionProductsDiffusionSolver](@ref Foam.fissionProductsDiffusionSolver).

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Formulation </summary> -->

#### Formulation

The diffusion coefficient is calculated in an Arrhenius way .

$$
\begin{aligned}
D = D_0 exp{\left( \frac{-Q_0}{RT} \right)} + D_1 exp{\left( \frac{-Q_1}{RT} \right)}
\end{aligned}
$$

where:

- \\( R \\) is the universal gas constant,
- \\( D_0 \\), \\( D_1 \\), \\( Q_0 \\), \\( Q_1 \\) are specific values for distinct species within various layers. The value can be found in the reference.

<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> Usage </summary> -->

#### Usage

To set the `diffCoefModel` in OFFBEAT, the user will need to specify the model in the material dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
	diffCoefModel			kernelArrhenius;
</code></pre>
</div>

The available models are listed in the table below:

| Materials	|	Default Model |
|-----------|---------------|
|	UO2	|	[kernelArrhenius](@ref Foam.diffCoefKernel)	|
|	PyC |	[PyCArrhenius](@ref Foam.diffCoefPyC)	|
|	SiC	| [SiCArrhenius](@ref Foam.diffCoefSiC)	|
|	graphite	|	[graphiteArrhenius](@ref Foam.diffCoefGraphite)	|
|	others	|	[inputArrhenius](@ref Foam.diffCoefArrhenius) |

No additional settings are required for the simulation for **kernelArrhenius**, **PyCArrhenius**, **SiCArrhenius** and **graphiteArrhenius**. **inputArrhenius** need users to specify the parameters \\( D_0 \\), \\( D_1 \\), \\( Q_0 \\), \\( Q_1 \\) for the correlation above.

<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; overflow: scroll; height: 200px; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
  fissionProductsTransport
  {
    fissionProductsName  ("Cs" "Ag");
    Cs
    {
      d1        1e-8;
      d2        0;	//default value 0
      g1        0;
      g2        0;
    }
    Ag
    {
      d1        1e-8;
    }
  }
</code></pre>
</div>


<div class="border-box" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color:#cfe3f5; color:#05134a'>
<b>Note</b>
<br>User can also provide value for other models such as **PyCArrhenius**. In that case, User-provided values take precedence. For instance, if the PyC model **PyCArrhenius** is applied, but the diffusion coefficient of Cs is manually set to 1e-6, then the value of 1e-6 will be used instead of relying on the Cs correlation within PyC.
</div>



<!-- </details> -->

<!-- <details> -->
  <!-- <summary style="font-size: 0.9rem; font-weight: bold;"> References </summary> -->

#### References

  - Collin B P. Diffusivities of Ag, Cs, Sr, and Kr in TRISO fuel particles and graphite[R]. Idaho National Lab.(INL), Idaho Falls, ID (United States), 2016.

<!-- </details> -->
