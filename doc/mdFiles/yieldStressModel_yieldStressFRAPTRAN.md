### User documentation for `yieldStressConstant` class {#yieldStressModel_yieldStressConstant}

#### Description
The `hardening` allows the user to specify the yield stress as a function of the equivalent plastic strain.
It is intended to be used with the 'misesPlasticity' constitutive mechanical behaviour law.
The interpolation between each points of the table is linear. It is recommended to carefully discretize the evolution of the yield stress on a case by case basis.

#### Usage
To specify the yield stress as a function of the equivalent plastic strain, one must use the following syntyx in the solverDict:
</code></pre>
</div>	
<div class="text-block" style='padding:0.1em; margin-left: 4em; margin-right: 8em; border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
...
rheologyModel misesPlasticity;
rheologyModelOptions
{
    yieldStressModel    hardening;
    plasticStrainVsYieldStress table
    (
        (0.0 100e6)
        (0.05 250e6)
        (0.1 300e6)
    );
}
</code></pre>
</div>	