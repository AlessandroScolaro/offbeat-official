### User documentation for `diffusionSolver` class {#neutronicsSubSolver_diffusionSolver}

For general instructions on the modeling of neutronics in OFFBEAT, refer to [here](neutronics.html).

In the `diffusionSolver` solver, the neutron flux distribution is obtained from the solution of the one-group neutron diffusion equation using fitted parameters.

#### Formulation

The neutron diffusion solver solves the following equation:

$$
\begin{aligned} 
 D\nabla^2 \Phi = \Sigma_A\Phi
\end{aligned}
$$

where:

- \f$D\f$ is the neutron diffusion coefficient
- \f$\Phi\f$ is the neutron thermal flux 
- \f$\Sigma_A\f$ is the macroscopic absorption cross-section

The obtained thermal neutron flux shape is used by the class @ref Foam.burnupLassmann to compute the intra-pin power distribution. The solver considers the fuel pin to be a strongly absorbing medium for thermal neutrons, and therefore only diffusion and absorption terms appear in the equation. The boundary condition for neutron thermal flux needed by the solver can be selected by the user to a value of choice. Since only the thermal neutron shape is of importance for @ref Foam.burnupLassmann, the user could simply select the boundary condition for flux to be a `fixedValue` to 1. 

#### Usage

To use the `diffusion` solver in OFFBEAT, the user needs to specify it in the `solverDict` dictionary using the following syntax:

<div class="text-block" style='padding:0.1em; margin-left: 4em;  margin-right: 8em;  border: 1px solid gray; background-color: black; color: white;'>
<pre style="margin: 0;"><code>
   neutronicsSolver    diffusion;

</code></pre>
</div>