# OFFBEAT Documentation {#homePage}
OpenFOAM Fuel BEhavior Analysis Tool (OFFBEAT) is a three-dimensional finite-volume nuclear fuel performance code based on the [OpenFOAM® C++ library](https://openfoam.org/). The first version of OFFBEAT is essentially the product of the founding research of Scolaro \cite ScolaroThesis \cite ScolaroOffbeatNED. Building on the works of Jasak, Weller, Tuković, Cardiff and Clifford \cite JasakWellerLinearElasticity \cite TukovicFluidSolid \cite Cardiff30 \cite CliffordLocalPeakingBWR, OFFBEAT is developed according to a cell-centered finite-volume framework for total Lagrangian, small strain solid mechanics. This is combined with a framework for thermal analysis and with numerical developments concerning the treatment of the gap heat transfer and contact, based on a mapping algorithm that allows the use of independent non-conformal meshes for fuel and cladding. The code considers the temperature and burnup dependence of the material properties, and it can model fuel densification, relocation, swelling, growth, fission gas release, creep, plasticity, and other relevant fuel behavior phenomena.
OFFBEAT is a joint development by the [Laboratory of Reactor Safety (LRS)](https://www.epfl.ch/labs/lrs) at École Polytechnique Fédérale de Lausanne (EPFL) and [Laboratory for Reactor Physics and Thermal-Hydraulics (LRT)](https://www.psi.ch/en/lrt) at the Paul Scherrer Institut (PSI).

This wiki provides the basic documentation for OFFBEAT, including the following:

- [Code Theory](@ref theoryManual)
- [Code Installation](@ref installationGuide)
- [User Manual](@ref userManual)
- [References](@ref citelist)
