import numpy as np
import subprocess
import os

dirlist = [ item for item in os.listdir('./') if os.path.isdir(os.path.join('./', item)) ]
dirlist

enrichments = dirlist

for j in range(0,len(enrichments)):

	fname=str(enrichments[j])+'/UO2pellet_mdx1.m'

	# Extract XS_0 vector from the result file and store in .tmp file
	command = 'cat '+fname+' | grep -A 1000000 "XS_0" > '+fname+'.tmp'
	subprocess.check_output(command, shell=True)

	# Store XS_0 data
	XS_0 = np.genfromtxt(fname+'.tmp', skip_header=1, skip_footer=1)

	# Delete .tmp file
	os.remove(fname+'.tmp')

	# Initialize fission and capture xs arrays
	xs_f = []
	xs_c = []

	# Initialize ID nuclides
	idNuclides = [ 	'iU235',
					'iU236',
					'iU238',
					'iNp237',
					'iPu238',
					'iPu239',
					'iPu240',
					'iPu241',
					'iPu242',
					'iAm241',
					'iAm243',
					'iCm242',
					'iCm243',
					'iCm244'
				  ]

	for i in range(0,len(XS_0[:,0])):
		# Fission xs
		if(XS_0[i,1] == 18):
			xs_f.append(XS_0[i,3])

		# Capture xs
		if(XS_0[i,1] == 101):
			xs_c.append(XS_0[i,3])

	f = open('enrichment_'+str(enrichments[j]), 'w')
	f.write('xs data from file : '+fname+'\n')

	f.write('// Fission :\n')
	for i in range(0,len(xs_f)):
		f.write('sf_['+idNuclides[i]+'] = '+str(xs_f[i])+'e-24;\n')

	f.write('// Capture :\n')
	for i in range(0,len(xs_f)):
		f.write('sc_['+idNuclides[i]+'] = '+str(xs_c[i])+'e-24;\n')

	f.write('\n')
	f.close()

