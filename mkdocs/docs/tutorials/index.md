# **Tutorials**

![Image title](https://upload.wikimedia.org/wikipedia/commons/2/20/UnderCon_icon.svg)
**This page is a work in progress!!!**

In this tutorial, users will have access to the examples of different cases, for example, standard fuel rods case, TRISO fuel particles case, etc. In each of these cases, users will learn about the general workflow to simulate a case.

The different cases are:

* [MOX]
* [RIA]
* [Traditional Fuel rod]
* [TRISO](@ref tutorials_TRISO)

\htmlinclude ./tutorials_TRISO.md

***

Return to [User Manual](@ref userManual)
