# **User's Guide**
Welcome to the OFFBEAT User’s Guide! The following pages will guide you through the essential aspects of using OFFBEAT to perform fuel behavior simulations.

???+ warning

	The usage instructions in this guide assume that the user has a basic understanding of OpenFOAM usage, including the basic workflow (mesh-generation, preprocessing, running solvers and postprocessing). The user should understand the basic dictionary format for OpenFOAM.

    We recommend that new users work through the [OpenFOAM v9 User Guide](https://cfd.direct/openfoam/user-guide/) from the OpenFOAM foundation or the equivalent [OpenFOAM v2312 User Guide](https://doc.openfoam.com/2312/) from the ESI group before attempting to use OFFBEAT.

--- 

## **Contents**

1. [Folder Structure](general/folder_structure.md)
2. [General Workflow](general/workflow.md)
3. [Mesh](general/computational_domain.md)
4. [Setting up the 'solverDict'](solverDict/index.md)
5. [Material Models](materials/index.md)
6. [Boundary Conditions](boundary_conditions/index.md)
7. [Solution Control](solution_control/index.md)
8. [Time Control](time_control/index.md)
9. [Discretization Schemes](discretization_schemes/index.md)
10. [Data Procesing](data_processing/index.md)