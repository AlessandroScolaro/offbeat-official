# **Heat Source**

The <code><b>heatSource</b></code> class is used to enable the modeling of a heat source in OFFBEAT.

The power density or heat source density field is by default named <code><b>Q</b></code> and is in <code><b>W/m<sup>3</sup></b></code>.

???+ note

    Traditional 1D codes typically require as an input the radially averaged linear heat generation rate (or <code><b>lhgr</b></code>) as a function of time, and often allow the user to provide an axial profile. For 3D codes with arbitrary geometries and unstructured meshes like OFFBEAT, it is less straightforward to define the heat source field or the power density field. 
    
    For scenarios where the power density field is not symmetric, the simplest way to define the heat source field is to couple OFFBEAT with a neutronics/multiphysics solver that directly provides the 3D field and use the <code><b>[fromLatestTime](../../classes/physicsSubSolvers/heatSource/fromLatestTime.md)</b></code> heat source model. 
    Alternatively, one can define the power density field using OpenFOAM tools such as topoSet (e.g. for creating fields of heat source that can be modeled as mathematical functions). 
    
    On the other hand, for simulations (even in 3D) where the heat source is assumed to be uniform along the azimuthal angle, one can use the <code><b>lhgr</b></code> models developed specifically for OFFBEAT. These models are listed below.

The heat source model can be selected with the <code><b>heatSource</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code> dictionary, located in the <code><b>constant</b></code> folder).

## <b>Classes</b>

Currently, OFFBEAT supports the following heat source models:

- [none](../../classes/heatSource/heatSource.md) - it neglects heat source, i.e. the field `Q`is not created. **This is the default choice in OFFBEAT if the user does not specify the `heatSource` model in the `solverDict`**;
- [constant](../../classes/heatSource/constantHeatSource.md) - the volumetric heat source field is read from the <code><b>Q</b></code> file in the starting time folder. **The old name for this class `fromLatestTime` is now deprecated**;
- [constantLhgr](../../classes/heatSource/constantLhgr.md) - imposes a <b>constant</b> linear heat generation rate (with the possibility of adding a time-dependent radial/axial profile);
- [timeDependentLhgr](../../classes/heatSource/timeDependentLhgr.md) - imposes a <b>time-dependent</b> linear heat generation rate (with the possibility of adding a time-dependent radial/axial profile);
- [timeDependentVhgr](../../classes/heatSource/timeDependentVhgr.md) - imposes a <b>time-dependent</b> volumetric heat source or volumetric heat generation rate.
