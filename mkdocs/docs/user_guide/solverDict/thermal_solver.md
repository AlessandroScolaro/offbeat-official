# <b>Thermal Solver</b>
The <code><b>thermalSubSolver</b></code> class is used to handle the modeling of heat transfer in OFFBEAT.

The primary field in the thermal solver is the **temperature**, represented by `<b>T</b>` and expressed in `<b>K</b>`. The corresponding temperature field file must be present at the start of the simulation to set both the internal field and boundary conditions accordingly.

The thermal solver <u>must</u> be selected with the <code><b>thermalSolver</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code>  dictionary, located in the <code><b>constant</b></code>  folder). 

## <b>Classes</b>

Currently OFFBEAT supports the following thermal solvers:

- [constant](../../classes/physicsSubSolvers/thermalSubSolver/thermalSubSolver.md) - deactivates the thermal solution (i.e. no equation is solved) and the temperature <code><b>T</b></code> file is defined in the starting time folder. **The old name for this class `fromLatestTime` is now deprecated**;
- [readTemperature](../../classes/physicsSubSolvers/thermalSubSolver/readTemperature.md) - at each time-step the temperature field is read from a sub-folder in the <code>constant/</code> folder;
- [solidConduction](../../classes/physicsSubSolvers/thermalSubSolver/solidConductionSolver.md) - solves the heat conduction equation with the possibility of adding a volumetric source <code><b>Q</b></code>;