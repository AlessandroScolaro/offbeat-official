# Elements Transport Solution {#elementTransport}

![Image title](https://upload.wikimedia.org/wikipedia/commons/2/20/UnderCon_icon.svg)
**This page is a work in progress!!!**

OFFBEAT features various transport solvers for the solution of elements redistribution within the fuel rod domain. If one (or more) of these solvers are enabled by the user, the solution of one (or more) PDEs will be included in the OFFBEAT solution loop. 

### Usage
The element transport solver <u>must</u> be selected with the <code><b>elementTransport</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code>  dictionary, located in the <code><b>constant</b></code>  folder). 
<br>

The two following classes allow one to handle element transport solvers in the OFFBEAT simulation:

- [fromLatestTime](@ref Foam.elementTransport), that disables all the element transport solvers.
- [byList](@ref Foam.elementTransportByList), that allows one to specify a list of element transport solvers that the user wants to include in the fuel performance simulation. The following transport solvers are currently implemented in OFFBEAT:

	- [porosityTransport](@ref Foam.porosityTransportSolver), for the Porosity redistribution in fuel pins.
	- [PuRedistribution](@ref Foam.PuRedistributionSolver), for the Plutonium redistribution during irradiation in fuel pins.
	- [AmRedistribution](@ref Foam.AmRedistributionSolver), for the Americium redistribution during irradiation in fuel pins.

***

Return to [User Manual](@ref userManual)
