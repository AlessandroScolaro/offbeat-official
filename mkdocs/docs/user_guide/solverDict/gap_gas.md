# <b>Gap Gas Model</b>
The gap gas model handles the evolution of gap volume, pressure, temperature and composition. The specific gapGas model 

The gap gas model type can be selected with the <code><b>gapGas</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code>  dictionary, located in the <code><b>constant</b></code>  folder). 

???+ warning
    
    In the gapGas model the gap volumes typically refer to the portion of the rod actually modeled i the geometry. To illustrate this, let us consider a rod plenum volume of 36 cm3. If the rod is reproduced with a 2-degree wedge mesh, the plenum volume in the model is:
    
    $$
        V_{model plenum} = \frac{2}{360}\cdot\frac{36}{1e6} m^3 = 2 \cdot 10^{-07} m^3    
    $$

    For TRISO fuels, let us consider a full sphere gap of 36 cm3. If the sphere is reproduced
	with a 2-degree wedge mesh (in 1D), its fraction compared with the whole sphere is

	$$
	    fraction = \frac{(2\pi/180)^2}{4\pi}=9.696273622\cdot10^{-5}
	$$
	
	So the gap volume in this TRISO model is
    
	$$
        V_{model} = 36\cdot10^{-6}\cdot9.696273622\cdot10^{-5} m^3 = 3.49\cdot10^{-9} m^3
	$$

## <b>Classes</b>

Currently OFFBEAT supports the following gap gas models:

- [none](../../classes/gapGasModel/gapGasModel.md) - This model neglects the presence of a gap gas model. **This is the default choice in OFFBEAT if the user does not specify the `gapGas` model in the `solverDict`**..
- [FRAPCON](../../classes/gapGasModel/gapFRAPCON.md) - This model takes into account the evolution of the gap and plenum volume, pressure and temperature. The calculation of the gap gas properties is mostly derived from FRAPCON.
- [TRISO](../../classes/gapGasModel/gapTRISO.md) - Similar to the previous model but applied to TRISO buffer-to-IPyC gap.


