# <b>Axial Slice Mapper (3-D to 1-D)</b>

Although OFFBEAT is a multi-dimensional code supporting unstructured meshes and arbitrary geometries, certain behavioral models in fuel performance, such as correlations for calculating relocation strains or the `Lassman` burnup model, are rooted in traditional 1.5-D approaches. These models often assume the geometry is organized into axial slices and may require slice-averaged quantities (e.g., burnup or heat source density) for accurate results.

This approach also applies when running OFFBEAT in 1.5-D mode with the `modifiedPlaneStrain` option enabled in the `rheologyOptions` subdictionary. In this case, the axial force balance, necessary to determine the axial strain component, must be calculated slice-by-slice to ensure comparability with traditional codes.

However, OpenFOAM does not natively support the concept of axial slices, nor can this approach be generalized to arbitrary 3D domains with unstructured meshes. To address this, OFFBEAT includes a `sliceMapper` class that allows users to define 1-D axial slices superimposed on the 3-D mesh, specifying the method used for this mapping.

???+ warning

    These slices are not actual subdivisions of the geometry; instead, the `sliceMapper` creates a virtual mapping by storing the address of the cells that belong to each slice. This enables other OFFBEAT functionalities to perform operations over these slices as though they were physically present.

The axial slice mapper model can be selected with the <code><b>sliceMapper</b></code> keyword in the main dictionary of OFFBEAT (i.e. the <code><b>solverDict</b></code>  dictionary, located in the <code><b>constant</b></code>  folder). 

## <b>Classes</b>

Currently, OFFBEAT supports the following axial slice mappers:

- [none](../../classes/sliceMapper/sliceMapper.md) - As the name implies, no mapper is considered. Errors might occur if models are activated that require the presence of a mapper. **This is the default choice in OFFBEAT if the user does not specify the `mapper` model in the `solverDict`**;
- [byMaterial](../../classes/sliceMapper/sliceMapperByMaterial.md) - The number axial slices for each material are defined inside the respective dictionary inside the `materials` subdictionary of the `solverDict`. The user can define the height of each slice.
- [byPellets](../../classes/sliceMapper/sliceMapperByPellets.md) - The axial slices are defined pellet-wise by homogeneously dividing the total fuel stack height by a user-defined pellet height.
- [autoAxialSlices](../../classes/sliceMapper/sliceMapperAutoAxialSlices.md) - Axial slices are automatically generated for each material based on the axial position of each mesh cell. All cells belonging to the same material and sharing the same axial position are grouped into a single slice, allowing for structured operations across slices within the 3-D mesh.
