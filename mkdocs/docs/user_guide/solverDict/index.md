# <b>Configuration of Physics Models in `solverDict`</b>

The main options and parameters that define the physical models, material properties, and historical variables (such as irradiation history) are all specified in a single input file called `solverDict`, located in the `constant` subdirectory of the case.

The `solverDict` is organized into three main sections:

1. **Global Options and Switches**: This first section contains global settings and switches to select the physics and models to be used (e.g., type of thermal solver, burnup model, heat source model, etc.).

2. **Physics and Model Subdictionaries**: This section comprises subdictionaries for each activated physics or model. Not all physics or models require specific inputs or parameters; however, if they do, general options (i.e., those not specific to a particular material) are set here.

3. **Materials Subdictionary**: The final section, the `materials` subdictionary, includes a list of subdictionaries, with one assigned to each `cellZone` defined in the mesh. Each `cellZone` must be associated with a material model (though materials do not have to be unique), along with parameters and models specific to that material.

Here below you can find a simplified example of a typical `solverDict` file:

```cpp
//- Main switches and solver selection:
thermalSolver           solidConduction;
mechanicsSolver         smallStrain;
neutronicsSolver        diffusion;
elementTransport        none;

heatSource              constant;
fastFlux                constant;
burnup                  Lassmann;
mapper                  byMaterial;
fgr                     SCIANTIX;

//- Dictionaries dedicated to each physics/model:
mechanicsSolverOptions
{
    forceSummary on;
    // Other options etc.
}

heatSourceOptions
{
    // Other options etc.
}

// Other physics dictionaries etc.

//- Material subdictionaries, one per cellZone
materials
{
    fuel
    { 
        material UO2;
        enrichment 0.04;

        conductivityModel  UO2MATPRO;
        // Other options etc.
    }

    cladding
    {
        material zircaloy;
        // Other options etc.    
    }

    // Other materials if present etc.
}
```

___________________________________________________________________

In this part of the user guide, we’ll explore the various components of the `solverDict`, with a dedicated section for all physics and models that can be applied in an OFFBEAT simulation.

Material-specific models and properties (such as the specific conductivity model, material type, mechanical law, etc.) will be discussed in the next section of the user guide.

## **Contents**

1. [Thermal Solver](thermal_solver.md)
2. [Mechanics Solver](mechanics_solver.md)
    - 2.1. [Multi Material Interface](multi_material_correction.md)
3. [Neutronics Solver](neutronics_solver.md)
4. [Element Transport Solver](element_transport_solver.md)
5. [Gap Gas Model](gap_gas.md)
6. [Heat Source](heat_source.md)
7. [Fast Flux](fast_flux.md)
8. [Burnup](burnup.md)
9. [Fission Gas Release](fgr.md)
10. [Rheology](rheology.md)
11. [Axial Slice Mapper](slice_mapper.md)