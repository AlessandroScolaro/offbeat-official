///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 1.4                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//           L. Cognini                  //
///////////////////////////////////////////

#include "GlobalVariables.h"
#include "GasProduction.h"
#include "HeliumProduction.h"
#include "GasDiffusion.h"
#include "IntraGranularBubbleEvolution.h"
#include "IntraGranularGasSwelling.h"

void IntraGranularGasBehavior( );
