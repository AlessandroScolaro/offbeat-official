///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 0.1                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include "GlobalVariables.h"
#include "Solver.h"
#include "GrainBoundaryVacancyDiffusionCoefficient.h"

void InterGranularBubbleEvolution( );
