///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 1.4                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include <cmath>
#include <vector>
#include "GlobalVariables.h"
#include "ErrorMessages.h"

double GasDiffusionCoefficient(double temperature, double fission_rate);
