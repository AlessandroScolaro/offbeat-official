///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 1.4                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include "ManufacturedSolution.h"
#include "ManufacturedCoefficient.h"
#include "Solver.h"
#include "GasDiffusionCoefficient.h"
#include <iostream>
#include <fstream>
#include <vector>

//#include "GlobalVariables.h"

void SolverVerification( );
