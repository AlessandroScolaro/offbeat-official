/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::fromBurnupRadialProfile

Description
    Class to prescribe a radial profile as predicted by the burnup module.

Usage
    \verbatim
    heatSourceOptions
    {
        #include "$FOAM_CASE/constant/lists/lhgr"
        timeInterpolationMethod linear;

        axialProfile
        {
            type flat;
        }

        radialProfile
        {
            type    fromBurnup;
        }

        materials ( fuel );
    }
    \endverbatim

SourceFiles
    fromBurnupRadialProfile.C

\mainauthor
    A. Scolaro - EPFL (ECOLE POLYTECHNIQUE FEDERALE DE LAUSANNE, Switzerland, 
    Laboratory for Reactor Physics and Systems Behaviour)

\contribution
    E. Brunetto, C. Fiorina - EPFL\n
    I. Clifford - PSI (Paul Scherrer Institut, Switzerland)

\date 
    November 2021

\*---------------------------------------------------------------------------*/

#ifndef fromBurnupRadialProfile_H
#define fromBurnupRadialProfile_H

#include "radialProfile.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                         Class fromBurnupRadialProfile Declaration
\*---------------------------------------------------------------------------*/

class fromBurnupRadialProfile
:
    public radialProfile
{
    // Private Member Functions

        //- Disallow default bitwise copy construct
        fromBurnupRadialProfile(const fromBurnupRadialProfile&);

        //- Disallow default bitwise assignment
        void operator=(const fromBurnupRadialProfile&);

public:

    //- Runtime type information
    TypeName("fromBurnup");

    
    // Constructors

        //- Construct from dictionary
        fromBurnupRadialProfile
        (
            const fvMesh& mesh,    
            const dictionary& dict,
            const labelList& addr,
            const vector& axialDirection,
            scalar rMax 
        );

        
    //- Destructor
    virtual ~fromBurnupRadialProfile();

    // Member Functions

        //- Update the profile
        virtual void correct();
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
